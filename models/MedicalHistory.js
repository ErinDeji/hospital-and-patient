//Import the mongoose module
var mongoose = require("mongoose");

//Define a schema
var Schema = mongoose.Schema;
var medicalHistory = new Schema(
  {
    patientID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "patient",
    },
    Comment: { type: String },
    Recommendation: { type: String },
    Medications: { type: String },
    MedicalProblems: { type: String },
    Allergies: { type: String },
  },
  { timestamps: true }
);

//Export function to create "SomeModel" model class
module.exports = mongoose.model("history", medicalHistory);
