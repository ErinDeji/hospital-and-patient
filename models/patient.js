//Import the mongoose module
var mongoose = require("mongoose");

//Define a schema
var Schema = mongoose.Schema;
var patient = new Schema(
  {
    FirstName: { type: String },
    MiddleName: { type: String },
    Lastname: { type: String },
    DateofBirth: { type: String },
    Address: { type: String },
    Gender: { type: String },
    PhoneNumber: { type: String },
    BloodGroup: { type: String },
    GenoType: { type: String },
    Recordstatus: { type: String },
  },
  { timestamps: true }
);

//Export function to create "SomeModel" model class
module.exports = mongoose.model("patient", patient);
