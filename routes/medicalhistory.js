var express = require("express");
var router = express.Router();
// var mongoose = require('mongoose');
var Hosp = require("../models/MedicalHistory");

/* GET ALL */
router.get("/", function (req, res, next) {
  Hosp.find(function (err, medicalHistory) {
    if (err) return next(err);
    res.json(medicalHistory);
  });
});

/* GET BY ID */
router.get("/:id", function (req, res, next) {
  Hosp.findById(req.params.id, function (err, medicalHistory) {
    if (err) return next(err);
    res.json(medicalHistory);
  })
    .populate("patientID")
    .populate("medicalProblems");
});

/* SAVE */
router.post("/", function (req, res, next) {
  Hosp.create(req.body, function (err, medicalHistory) {
    if (err) return next(err);
    res.json(medicalHistory);
  });
});

/* UPDATE */
router.put("/:id", function (req, res, next) {
  Hosp.findByIdAndUpdate(req.params.id, req.body, function (
    err,
    medicalHistory
  ) {
    if (err) return next(err);
    res.json(medicalHistory);
  });
});

/* DELETE  */
router.delete("/:id", function (req, res, next) {
  Hosp.findByIdAndRemove(req.params.id, req.body, function (
    err,
    medicalHistory
  ) {
    if (err) return next(err);
    res.json(medicalHistory);
  });
});

module.exports = router;
