var express = require("express");
var router = express.Router();
// var mongoose = require('mongoose');
var Hosp = require("../models/patient");

/* GET ALL */
router.get("/", function getAll(req, res, next) {
  //get all items
  Hosp.find()
    .then((data) => res.json(data))
    .catch((err) => next(err));
});

/* GET BY ID */
router.get("/:id", function getById(req, res, next) {
  //get item by id
  Hosp.findById(req.params.id)
    .then((data) => (data ? res.json(data) : res.sendStatus(404)))
    .catch((err) => next(err));
});

/* SAVE */
router.post("/", function add(req, res, next) {
  //create new item
  Hosp.create(req.body)
    .then(() =>
      res.status(200).send({
        message: "Saved successfully",
      })
    )
    .catch((err) => next(err));
});

/* UPDATE */
router.put("/:id", function update(req, res, next) {
  //update item
  Hosp.findByIdAndUpdate(req.params.id, req.body)
    .then(() =>
      res.json({}).send({
        message: "Updated successfully",
      })
    )
    .catch((err) => next(err));
});

/* DELETE  */
router.delete("/:id", function _delete(req, res, next) {
  //delete item
  Hosp.findByIdAndRemove(req.params.id)
    .then(() =>
      res.json({}).send({
        message: "delete successfully",
      })
    )
    .catch((err) => next(err));
});

module.exports = router;
